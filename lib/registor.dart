import 'package:flutter/material.dart';
import 'package:sqliteflutter/showdata.dart';

import 'database/database_model.dart';
import 'model.dart';

class FromRegitor extends StatefulWidget {
  FromRegitor();

  @override
  _FromRegitorState createState() => _FromRegitorState();
}

class _FromRegitorState extends State<FromRegitor> {
  var name = TextEditingController();
  var username = TextEditingController();
  var password = TextEditingController();
  var age = TextEditingController();
  var role = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Register"),
        ),
        body: SingleChildScrollView(
            child: Container(
                child: Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
              Text(
                'Register',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
              ),
              Container(
                width: 350,
                child: TextField(
                  controller: name,
                  decoration: InputDecoration(
                      labelText: 'Name',
                      filled: true,
                      fillColor: Colors.white,
                      border: OutlineInputBorder()),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: 350,
                  child: TextField(
                    controller: username,
                    decoration: InputDecoration(
                        labelText: 'Username',
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder()),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: 350,
                  child: TextField(
                    controller: password,
                    decoration: InputDecoration(
                        labelText: 'Password',
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder()),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: 350,
                  child: TextField(
                    controller: age,
                    decoration: InputDecoration(
                        labelText: 'Role',
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder()),
                  ),
                ),
              ),
              ElevatedButton(
                  child: const Text('Add'),
                  onPressed: () async {
                    await DatabaseModel.instance.add(RegisterModel(
                        name: name.text,
                        username: username.text,
                        password: password.text,
                        age: age.text,
                        role: role.text));
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => ShowData()));
                  })
            ])))));
  }
}
