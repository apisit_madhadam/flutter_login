import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../model.dart';

class DatabaseModel {
  DatabaseModel._privateConstructor();
  static final DatabaseModel instance = DatabaseModel._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'register.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE registerDB (
          id INTEGER PRIMARY KEY,
          name TEXT,
          username TEXT,
         password TEXT,
          age TEXT,
          role TEXT
      )
      ''');
  }

  Future<List<RegisterModel>> getGroceries() async {
    Database db = await instance.database;
    var groceries = await db.query('registerDB', orderBy: 'name');
    List<RegisterModel> groceryList = groceries.isNotEmpty
        ? groceries.map((c) => RegisterModel.fromMap(c)).toList()
        : [];
    return groceryList;
  }

  Future<List<RegisterModel>> getProfile(int id) async {
    Database db = await instance.database;
    var profile =
        await db.query('registerDB', where: 'id = ?', whereArgs: [id]);
    List<RegisterModel> profileList = profile.isNotEmpty
        ? profile.map((c) => RegisterModel.fromMap(c)).toList()
        : [];
    return profileList;
  }

  Future<int> add(RegisterModel grocery) async {
    Database db = await instance.database;
    return await db.insert('registerDB', grocery.toMap());
  }

  Future<RegisterModel?> getLoginUser(String uname, String upward) async {
    var db = await database;
    var res = await db.rawQuery('''
      SELECT * FROM registerDB WHERE
        
          username TEXT,
         password TEXT,
       
      )
      ''');
    if (res.length > 0) {
      return RegisterModel.fromMap(res.first);
    }
    return null;
  }
}
