class RegisterModel {
  int? id;
  String name;
  String username;
  String password;
  String age;
  String role;

  RegisterModel({
    this.id,
    required this.name,
    required this.username,
    required this.password,
    required this.age,
    required this.role,
  });

  factory RegisterModel.fromMap(Map<String, dynamic> json) => RegisterModel(
      id: json['id'],
      name: json['name'],
      username: json['username'],
      password: json['password'],
      age: json['age'],
      role: json['role']);

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'username': username,
      'password': password,
      'age': age,
      'role': role,
    };
  }
}
