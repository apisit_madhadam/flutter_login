import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqliteflutter/registor.dart';
import 'package:sqliteflutter/showdata.dart';

import 'database/database_model.dart';
import 'model.dart';

class LoginFrom extends StatefulWidget {
  const LoginFrom({Key? key}) : super(key: key);

  @override
  State<LoginFrom> createState() => _LoginFromState();
}

class _LoginFromState extends State<LoginFrom> {
  Future<SharedPreferences> _pref = SharedPreferences.getInstance();

  var dbHelper = DatabaseModel();
  var username = TextEditingController();
  var password = TextEditingController();

  login() async {
    String uname = username.text;
    String upward = password.text;

    if (uname.isEmpty) {
      throw ("Please Enter User ID");
    } else if (upward.isEmpty) {
      throw ("Please Enter Password");
    } else {
      await dbHelper.getLoginUser(uname, upward).then((userData) {
        if (userData != null) {
          (userData).whenComplete(() {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (_) => ShowData()),
                (Route<dynamic> route) => false);
          });
        } else {
          throw ("Error: User Not Found");
        }
      }).catchError((error) {
        print(error);
        throw ("Error: Login Fail");
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Login"),
        ),
        body: SingleChildScrollView(
            child: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Login',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                ),
                Container(
                  width: 350,
                  child: TextField(
                    controller: username,
                    decoration: InputDecoration(
                        labelText: 'Username',
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder()),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: 350,
                    child: TextField(
                      controller: password,
                      decoration: InputDecoration(
                          labelText: 'Password',
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder()),
                    ),
                  ),
                ),
                Container(
                  child: FlatButton(
                    minWidth: 350,
                    color: Colors.deepOrangeAccent,
                    onPressed: login,
                    child: Text("Login"),
                  ),
                ),
                Container(
                  child: FlatButton(
                    minWidth: 350,
                    color: Colors.blue,
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => FromRegitor()));
                    },
                    child: Text("Register"),
                  ),
                ),
                Container(
                  child: FlatButton(
                    minWidth: 350,
                    color: Colors.blue,
                    onPressed: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => ShowData()));
                    },
                    child: Text("Showdatabase"),
                  ),
                ),
              ],
            ),
          ),
        )));
  }

  Future setSP(RegisterModel user) async {
    final SharedPreferences sp = await _pref;

    sp.setString("username", user.username);
    sp.setString("password", user.password);
  }
}
