import 'package:flutter/material.dart';

import 'database/database_model.dart';
import 'model.dart';

class ShowData extends StatefulWidget {
  const ShowData({Key? key}) : super(key: key);

  @override
  State<ShowData> createState() => _ShowDataState();
}

class _ShowDataState extends State<ShowData> {
  int? selectedId;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FutureBuilder<List<RegisterModel>>(
            future: DatabaseModel.instance.getGroceries(),
            builder: (BuildContext context,
                AsyncSnapshot<List<RegisterModel>> snapshot) {
              if (!snapshot.hasData) {
                var grocery;
                return Center(
                  child: Text('${grocery.name} ${grocery.role}'),
                );
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Profiles in List.'))
                  : ListView(
                      children: snapshot.data!.map((grocery) {
                        return Container(
                            child: Card(
                          color: selectedId != null
                              ? Colors.white70
                              : Colors.white,
                          child: Stack(children: [
                            Container(),
                            Text(
                                '${grocery.id}  ${grocery.role} ${grocery.username}${grocery.password}${grocery.role}'),
                            Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[]),
                          ]),
                        ));
                      }).toList(),
                    );
            }),
      ),
      drawer: Drawer(
        child: ListView(padding: const EdgeInsets.all(0.0), children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: const Text(
              'Menu About',
              style: TextStyle(fontSize: 25),
            ),
            accountEmail: const Text('About up'),
          ),
        ]),
      ),
    );
  }
}
